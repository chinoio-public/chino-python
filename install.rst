
INSTALL
=======
.. code-block:: python

    pip install chino

USAGE
------
.. code-block:: python

    from chino.api import ChinoAPIClient
    chino = ChinoAPIClient(<customer_id>, <customer_key>)
